import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

function ListZodiac() {
	return (
		<div>
			<h1>Знаки зодіаку</h1>
			<table>
				<thead>
					<tr>
						<th>Знак зодіаку</th>
						<th>Символ</th>
						<th>Проміжок дат</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td className="znak">Овен</td>
						<td>
							<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/Aries.svg/20px-Aries.svg.png"
								alt="Овен" ></img>
						</td>
						<td className="dates">21 березня – 19 квітня</td>
					</tr>
					<tr>
						<td className="znak">Телець</td>
						<td>
							<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Taurus.svg/20px-Taurus.svg.png"
								alt="Телець" ></img>
						</td>
						<td className="dates">20 квітня – 20 травня</td>
					</tr>
					<tr>
						<td className="znak">Близнюки</td>
						<td>
							<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/15/Gemini.svg/20px-Gemini.svg.png"
								alt="Близнюки" ></img>
						</td>
						<td className="dates">21 травня – 20 червня</td>
					</tr>
					<tr>
						<td className="znak">Рак</td>
						<td>
							<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Cancer.svg/20px-Cancer.svg.png"
								alt="Рак" ></img>
						</td>
						<td className="dates">21 червня – 22 липня</td>
					</tr>
					<tr>
						<td className="znak">Лев</td>
						<td>
							<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Leo.svg/20px-Leo.svg.png"
								alt="Лев" ></img>
						</td>
						<td className="dates">23 липня- 22 серпня</td>
					</tr>
					<tr>
						<td className="znak">Діва</td>
						<td>
							<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Virgo.svg/20px-Virgo.svg.png"
								alt="Діва" ></img>
						</td>
						<td className="dates">23 серпня – 22 вересня</td>
					</tr>
					<tr>
						<td className="znak">Терези</td>
						<td>
							<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f7/Libra.svg/20px-Libra.svg.png"
								alt="Терези" ></img>
						</td>
						<td className="dates">23 вересня – 22 жовтня</td>
					</tr>
					<tr>
						<td className="znak">Скорпіон</td>
						<td>
							<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/ea/Scorpio.svg/20px-Scorpio.svg.png"	alt="Скорпіон" ></img>
						</td>
						<td className="dates">24 жовтня — 22 листопада</td>
					</tr>
					<tr>
						<td className="znak">Стрілець</td>
						<td>
							<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Sagittarius.svg/20px-Sagittarius.svg.png"	alt="Стрілець" ></img>
						</td>
						<td className="dates">23 листопада — 21 грудня</td>
					</tr>
					<tr>
						<td className="znak">Козоріг</td>
						<td>
							<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Capricorn.svg/20px-Capricorn.svg.png"	alt='Козоріг' ></img>
						</td>
						<td className="dates">22 грудня — 20 січня</td>
					</tr>
					<tr>
						<td className="znak">Водолій</td>
						<td>
							<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Aquarius.svg/20px-Aquarius.svg.png"
								alt="Водолій"	></img>
						</td>
						<td className="dates">21 січня — 18 лютого</td>
					</tr>
					<tr>
						<td className="znak">Риби</td>
						<td>
							<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Pisces.svg/20px-Pisces.svg.png"
								alt="Риби" ></img>
						</td>
						<td className="dates">19 лютого — 20 березня</td>
					</tr>
				</tbody>
			</table>
		</div>
	);
}

ReactDOM.render(<ListZodiac></ListZodiac>, document.querySelector("#start"));
